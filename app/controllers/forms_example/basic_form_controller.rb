class FormsExample::BasicFormController < ApplicationController
  helper_method :current_user

  def index
    @user = User.new(name: "Foo", age: 0, sex: "-")
    gon.push(
      user: @user
    )
  end

  def create
    gon.push({
      user: {
        name: params[:name],
        age: params[:age].present? ? params[:age].to_i : 0,
        sex: params[:sex]
      }
    })
  end

  private
  def current_user
    @user
  end
end
