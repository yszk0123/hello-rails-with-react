"use strict";
var assign = require("object-assign");

var commonConfig = require("./common");
var env = process.env.NODE_ENV || "development";
var webpack = require("./webpack-" + env);

module.exports = assign(
  {},
  commonConfig,
  { webpack: webpack }
);
