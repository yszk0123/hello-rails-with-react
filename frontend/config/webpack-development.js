"use strict";
var path = require("path");
var webpack = require("webpack");

var commonConfig = require("./common");
var collectEntries = require("./collectEntries");

module.exports = {
  devtool: "cheap-module-eval-source-map",
  node: {
    __dirname: false
  },
  entry: collectEntries(),
  output: {
    path: path.join(__dirname, "..", "..", "app", "assets", "javascripts"),
    filename: "[name].js",
    publicPath: "/assets/"
  },
  resolve: {
    extensions: ["", ".webpack.js", ".web.js", ".js", ".jsx", ".json"]
  },
  plugins: [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.DefinePlugin({
      // necessary for superagent to work in browser
      "global.GENTLY": false,

      "__DEVELOPMENT__": true,
      "process.env": {
        "NODE_ENV": JSON.stringify("development")
      }
    }),
    new webpack.NoErrorsPlugin()
  ],
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        loader: "babel-loader",
        include: [
          path.resolve(__dirname, "..", "src"),
          path.resolve(__dirname, "..", "test")
        ]
      }
    ]
  }
};
