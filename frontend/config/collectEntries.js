"use strict";
var fs = require("fs");
var path = require("path");
var commonConfig = require("./common");

var stripSuffix = function(path) {
  return path.split(".").slice(0, -1).join(".");
};

module.exports = function collectEntries() {
  var root = commonConfig.entryRootDir;

  return fs.readdirSync(root)
    .reduce(function(entries, dir) {
      var resolvedPath = require.resolve(`../${path.join(root, dir)}`);
      var name = stripSuffix(path.basename(dir));
      entries[name] = stripSuffix(resolvedPath);
      return entries;
    }, {});
};
