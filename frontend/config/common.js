"use strict";
var path = require("path");

var config = {
  port: Number(process.env.PORT) || 3000,
  buildTarget: ({
    web: "web",
    node: "node",
    electron: "electron"
  })[process.env.BUILD_TARGET] || "web",
  entryRootDir: "./src/entries",
  faviconPath: path.join(__dirname, "..", "src", "assets", "images", "favicon.ico")
};

module.exports = config;
