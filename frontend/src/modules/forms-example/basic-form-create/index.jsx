import React from "react";
import ReactDOM from "react-dom";

const Layout = ({ children }) =>
  <div>{children}</div>;

const StateDisplay = ({ state }) =>
  <div>{JSON.stringify(state)}</div>;

const Root = ({ state }) =>
  <Layout>
    <div>State</div>
    <StateDisplay state={state} />
  </Layout>;

export function mount(mountPoint, initialState) {
  ReactDOM.render(
    <Root state={initialState} />,
    mountPoint
  );
}
