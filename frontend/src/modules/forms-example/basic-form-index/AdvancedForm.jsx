import React from "react";

import { RailsForm } from "../../../common/rails-ui";

const CustomInput = ({ type, name, label, defaultValue }) =>
  <span>
    <label for={name}>{label}</label>
    <input type={type} name={name} defaultValue={defaultValue} />
  </span>;

const AdvancedForm = ({ user }) =>
  <RailsForm action="/forms_example/basic_form" method="post">
    <CustomInput type="text" name="name" label="Name" defaultValue={user.name} />
    <CustomInput type="text" name="age" label="Age" defaultValue={user.age} />
    <CustomInput type="text" name="sex" label="Sex" defaultValue={user.sex} />
    <input type="submit" name="commit" value="Submit" />
  </RailsForm>;

export default AdvancedForm;
