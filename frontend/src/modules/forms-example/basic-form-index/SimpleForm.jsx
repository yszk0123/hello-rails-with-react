import React from "react";

import { RailsForm } from "../../../common/rails-ui";

const SimpleForm = ({ user }) =>
  <RailsForm action="/forms_example/basic_form" method="post">
    <label for="name">Name</label>
    <input type="text" name="name" defaultValue={user.name} />
    <label for="age">Age</label>
    <input type="text" name="age" defaultValue={user.age} />
    <label for="sex">Sex</label>
    <input type="text" name="sex" defaultValue={user.sex} />
    <input type="submit" name="commit" value="Submit" />
  </RailsForm>;

export default SimpleForm;
