import React from "react";
import ReactDOM from "react-dom";

import SimpleForm from "./SimpleForm";
import AdvancedForm from "./AdvancedForm";

const Layout = ({ children }) =>
  <div>{children}</div>;

// TODO: Extract style
const DisplayBox = ({ label, children }) =>
  <div style={{ margin: "8px", padding: "8px", background: "lightgray" }}>
    <h3>{label}</h3>
    <div>{children}</div>
  </div>;

const Root = ({ state }) =>
  <Layout>
    <DisplayBox label="SimpleForm">
      <SimpleForm user={state.user} />
    </DisplayBox>
    <DisplayBox label="AdvancedForm">
      <AdvancedForm user={state.user} />
    </DisplayBox>
  </Layout>;

export function mount(mountPoint, initialState) {
  ReactDOM.render(
    <Root state={initialState} />,
    mountPoint
  );
}
