import React from "react";

import * as AuthUtils from "../auth-utils";

export const RailsForm = ({ action, method, children }) =>
  <form action={action} accept-charset="UTF-8" method={method}>
    <input name="utf8" type="hidden" value="✓" />
    <input type="hidden" name="authenticity_token" value={AuthUtils.getAuthenticityToken()} />
    {children}
  </form>;
