let authenticityToken;

export const getAuthenticityToken = () => {
  if (!authenticityToken) {
    authenticityToken = $('meta[name="csrf-token"]').attr("content");
  }

  return authenticityToken;
};
