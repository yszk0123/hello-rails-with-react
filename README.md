# Install & Setup

`$ bundle install`

`$ pushd frontend && npm install && npm run build && popd`

`$ bin/rake db:migrate`

`$ bin/rails s`

Open "http://localhost:3000/forms_example/basic_form"
