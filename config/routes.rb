Rails.application.routes.draw do
  namespace :forms_example do
    get "basic_form", to: "basic_form#index"
    post "basic_form", to: "basic_form#create"
  end
end
